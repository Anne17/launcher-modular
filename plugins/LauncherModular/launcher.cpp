#include <QDebug>
#include <QDirIterator>
#include <QStandardPaths>
#include <QCoreApplication>

#include "launcher.h"


LauncherModularPlug::LauncherModularPlug() {
    m_dashOverride = QStringLiteral("%1/upstart/unity8-dash.override").arg(
        QStandardPaths::writableLocation(QStandardPaths::ConfigLocation)
    );

    m_isDashSet = getIsDashSet();
    Q_EMIT isDashSetChanged();
}

bool LauncherModularPlug::isDashSet() const {
    return m_isDashSet;
}

bool LauncherModularPlug::getIsDashSet() {
    QFile file(m_dashOverride);
    if(!file.exists())
	    return false;
    else {
	QFile file(m_dashOverride);
    	if (!file.open(QIODevice::ReadOnly | QIODevice::Text)) {
        	return false;
	}
	QTextStream in(&file);
	while(!in.atEnd()) {
		QString line = in.readLine();
		if(line.contains("BINARY")) {
		       if(line.contains("launchermodular.ubuntouchfr")) {
			       file.close();
				return true;
			}
			else
			{
				file.close();
				return false;
			}
		}
	}
	file.close();
    }
    return true;
}

bool LauncherModularPlug::setDash() {
    QFile file(m_dashOverride);
    if (!file.open(QIODevice::WriteOnly | QIODevice::Text)) {
        return false;
    }

    QTextStream out(&file);

    out << "env BINARY=/opt/click.ubuntu.com/launchermodular.ubuntouchfr/current/launchermodular" << endl;
    out << "pre-start script" << endl;
    out << "initctl emit scope-ui-starting" << endl;
    out << "if [ ! -f \"/opt/click.ubuntu.com/launchermodular.ubuntouchfr/current/launchermodular\" ]" << endl;
    out << "then" << endl;
    out << "rm -f /home/phablet/.config/upstart/unity8-dash.override" << endl;
    out << "stop" << endl;
    out << "exit 0" << endl;
    out << "fi" << endl;
    out << "end script" << endl;
        
    file.close();

    m_isDashSet = true;
    Q_EMIT isDashSetChanged();

    return true;
}

bool LauncherModularPlug::unsetDash() {
    QFile file(m_dashOverride);
    if (!file.remove()) {
        return false;
    }

    m_isDashSet = false;
    Q_EMIT isDashSetChanged();

    return true;
}
