#!/bin/bash

#mount -o rw,remount /

if [ ! -f "/home/phablet/.config/upstart/unity8-dash.override" ]
then
	echo 'env BINARY=/opt/click.ubuntu.com/launchermodular.ubuntouchfr/current/launchermodular
pre-start script
    initctl emit scope-ui-starting
    if [ ! -f "/opt/click.ubuntu.com/launchermodular.ubuntouchfr/current/launchermodular" ]
        then
        rm -f /home/phablet/.config/upstart/unity8-dash.override
        stop
        exit 0
    fi
end script
' >/home/phablet/.config/upstart/unity8-dash.override
fi

#mount -o ro,remount /
