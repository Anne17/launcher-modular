import QtQuick 2.4
import QtQuick.Layouts 1.1
import QtGraphicalEffects 1.0
import Qt.labs.settings 1.0
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import MySettings 1.0
import AppHandler 1.0
import "pages"
import QtQuick.Controls 2.2
import AccountsService 0.1
    
MainView {
    id: launchermodular
    objectName: 'mainView'
    applicationName: 'launchermodular.ubuntouchfr'
    automaticOrientation: true

    width: units.gu(45)
    height: units.gu(75)


    function getPageArray() {
        var page = [], hM_l = launchermodular.pageModel.count;
        for (var i=0; i<hM_l; i++){
            var item = launchermodular.pageModel.get(i);
            page.push({"name": item.name, "icon": item.icon, "data":item.data, "directory":item.directory});
        }

        console.log("pageModel to array, size : "+page.length);
        return page;
    }

    property ListModel pageModel :  ListModel { id: pageModel }

/*
    function getCustomIconArray() {
        var customIcon = [], hM_l = launchermodular.customIconModel.count;
        for (var i=0; i<hM_l; i++){
            var item = launchermodular.customIconModel.get(i);
            customIcon.push({"name": item.name, "icon": item.icon, "action": item.action});
        }

        console.log("customIconModel to array, size : "+customIcon.length);
        return customIcon;
    }

    property ListModel customIconModel :  ListModel { id: customIcon }
*/
    // persistent app settings:
    property var settings: Settings {

        property bool firstRun: true
        property bool firstRunNew: true
        property bool developerModeEnabled:	false;

        property string apiOpenWeatherMap: ''
        property string unitsFormat: '&units=imperial'
        property string cityName: ''

        property string formatHours: '12h'
        property string backgroundAnalogHours: ''

        property string iconStyle: 'default'

        property string textColor: '#FFFFFF'
        property string backgroundColor: '#000000'
        property string backgroundOpacity: '0.7'
        property string backgroundBlur: '0'

        property int limiteDaysCalendar: 60;
        property int limiteDaysWidgetEvent: 60;
        property int limiteItemWidgetEvent: 3;

        property var page;
        //property var customIcon;

        property var selectedAppNews: [];
	signal newsConfigChanged();

        property bool newsBackgroundOpacity: false
        property string widgetMessageClick: 'default'
        property bool widgetMessageSummary: true
        property string widgetCallClick: 'default'
	
    }//settings

    property variant datenow: new Date()

        
    Timer {
        id: clockUpdater
        interval: 60000-(launchermodular.datenow.getSeconds()*1000) 
        running: true
        repeat: true
        onTriggered: {
            if(clockUpdater.interval != 60000){clockUpdater.interval = 60000}
            launchermodular.datenow = new Date()
        }
    }
    
    
PageStack {
  id: pageStack
    Page {
        id: pageLauncher
        anchors.fill: parent


        Component.onCompleted: {
            if(launchermodular.settings.firstRunNew){
                launchermodular.settings.page = undefined
                launchermodular.settings.firstRunNew = false
            }

            console.log("###### on Component completion #####")
            if(typeof launchermodular.settings.page === 'undefined') {
                console.log("page is undefined, let's create a new one");
                launchermodular.settings.page = [{"name": "Home.qml", "icon": "pages/home/assets/icon.svg","data":{}, "directory": "pages/"}];
            }
            console.log("Retrieve page with : "+ launchermodular.settings.page.length +" elemets");
            var page_l = launchermodular.settings.page.length
            for (var i=0; i<page_l; i++){
                var item = launchermodular.settings.page[i];
                launchermodular.pageModel.insert(i,{"name": item.name, "icon": item.icon, "data":item.data, "directory":item.directory})
            }

            /*
            console.log("Retrieve custom icon with : "+ launchermodular.settings.customIcon.length +" elemets");
            var customIcon_l = launchermodular.settings.customIcon.length
            for (var i=0; i<customIcon_l; i++){
                var item = launchermodular.settings.customIcon[i];
                launchermodular.customIconModel.insert(i,{"name": item.name, "icon": item.icon, "action": item.action})
            }*/
        }
        Component.onDestruction: {
            console.log("####### On component destruction ###### ");
            launchermodular.settings.page = getPageArray();
            console.log("Store page with : "+ launchermodular.settings.page.length +" elemets");

            //launchermodular.settings.customIcon = getCustomIconArray();
            //console.log("Store customIcon with : "+ launchermodular.settings.customIcon.length +" elemets");
        }

        header: PageHeader {
            id: header
            visible: false
        }
            
    
            Rectangle {
                id: topBorder
                height: units.gu(0.1)
                anchors.left: parent.left
                anchors.right: parent.right
                color: "#E95420"
            }

        Rectangle {
            id: background
            anchors {
                left: parent.left
                right: parent.right
                top: topBorder.bottom
                bottom: bottomBarLayout.top
            }

            Image {
                id: backgroundblur
                anchors.fill: parent;
                source: AccountsService.backgroundFile //unconfinedMySettings.getBackgroundFile() ? MySettings.getBackgroundFile() : "../assets/wallpaper.png"
                smooth: true
                fillMode: Image.PreserveAspectCrop
                visible: false
            }

    FastBlur {
        anchors.fill: backgroundblur
        source: backgroundblur
        radius: launchermodular.settings.backgroundBlur
    }
            Rectangle {
            id: listAppBackground
            anchors.fill: parent
            color: launchermodular.settings.backgroundColor
            opacity: launchermodular.settings.backgroundOpacity
            }

            Rectangle {
                id: listApp
                anchors.fill: parent
                color: "transparent"

/* ******************************** LES PAGES COMMENCE ICI ******************************** */




                SwipeView {
                    id: view

                    currentIndex: 0
                    anchors.fill: parent

    Repeater {
        model: launchermodular.pageModel
        Loader {
                sourceComponent:{
                    Qt.createComponent(directory+name)
                } 
	    property int pageIndex:index
			
        }
    }

                }


/* ******************************** FIN DES PAGES ******************************** */


            }
        }

/* ******************************** DEBUT DE LA BARRE EN BAS ******************************** */

        Rectangle {
            id: bottomBarLayout
            color: "#111111"
            height: units.gu(5)

            anchors {
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

            Rectangle {
                id: borderTop
                height: units.gu(0.1)
                anchors.left: parent.left
                anchors.right: parent.right
                color: "#E95420"
            }

MouseArea {
		anchors.fill: parent
		property real startMouseX
		//onEntered: {
		//	startMouseX = mouseX
		//}
		onPressed: {
			startMouseX = mouseX
		}
		onPositionChanged: {
			if(mouseX - startMouseX > units.gu(4)) {
				view.setCurrentIndex((view.currentIndex+1)%view.count)
				startMouseX += units.gu(4)
			}
			else if(startMouseX - mouseX > units.gu(4)) {
				view.setCurrentIndex((view.currentIndex-1+view.count)%view.count)
				startMouseX -= units.gu(4)
			}
		}

		onExited: {indicator.spacing = units.gu(2)}

PageIndicator {
        id: indicator
        count: view.count
        currentIndex: view.currentIndex
        interactive: false //= dont intercept press
        anchors.horizontalCenter: parent.horizontalCenter
        anchors.verticalCenter: parent.verticalCenter
        spacing: units.gu(2)

            delegate: Rectangle {
                height: units.gu(4)
                width: units.gu(3)
                color: "transparent"
            Icon {
                anchors.horizontalCenter: parent.horizontalCenter
                anchors.verticalCenter: parent.verticalCenter
                source: launchermodular.pageModel.get(index).icon
                height: units.gu(2)
                width: units.gu(2)
                color: index === view.currentIndex ? "#E95420" : pressed ? "#000000" : "#FFFFFF"
            }

          MouseArea {
                    anchors.fill: parent
                    propagateComposedEvents: true
                      onPressed: {
			             mouse.accepted = false
                        if(index !== view.currentIndex) {
                            view.setCurrentIndex(index);
                            source = indicator.indicatorIcons[view.currentIndex];
                        }
                     }

            }
        }


}
	}
            
Rectangle {
        id: tutorialConfig
        visible: launchermodular.settings.firstRun
        color:"#111111"
        height: units.gu(5)
            anchors {
                top: bottomBarLayout.top
                topMargin: 2
                left: parent.left
                right: parent.right
                bottom: parent.bottom
            }

    
            Icon {
                anchors.right: staticText.left
                anchors.rightMargin: units.gu(1)
                anchors.verticalCenter: parent.verticalCenter
                width: units.gu(2)
                height: units.gu(2)
                name: "up"
            }    
    
            Text {
                id:staticText
                anchors.verticalCenter: parent.verticalCenter
                anchors.horizontalCenter: parent.horizontalCenter
                color: "darkgray"
                text: "Swipe up to configure the launcher"
            }

            Icon {
                anchors.left: staticText.right
                anchors.leftMargin: units.gu(1)
                anchors.verticalCenter: parent.verticalCenter
                width: units.gu(2)
                height: units.gu(2)
                name: "up"
            }
            
    }            
            
        transform: Translate {
            y: -(bottomBarSettings.position * bottomBarSettings.height)
        }

        }
    Drawer {
        id: bottomBarSettings
        edge: Qt.BottomEdge
        height: units.gu(8)
        width: parent.width
            
        onOpened: launchermodular.settings.firstRun = false
            
            Row {
                spacing: 4
                anchors.verticalCenter: parent.verticalCenter
                anchors.right: parent.right
                height: units.gu(5)
                /*
                Column {
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height
                    width: units.gu(8)
                    Icon {
                        id: iconShortcut
                        anchors.horizontalCenter: parent.horizontalCenter
                        name: "keypad"
                        height: units.gu(3)
                        width: units.gu(3)
                    }
                    Text {
                        anchors.top: iconShortcut.bottom
                        horizontalAlignment: Text.AlignHCenter
                        width: parent.width
                        text: i18n.tr("Sets shortcut")
                        font.pointSize: units.gu(1)
                        wrapMode:Text.WordWrap
                    }
                  MouseArea {
                            anchors.fill: parent
                            onPressed: {
                                    //pageStack.push(Qt.resolvedUrl("Widgetmanagement.qml"))
                                    bottomBarSettings.close()
                            }
                  }
                }
                Column {
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height
                    width: units.gu(8)
                    Icon {
                        id: iconWidget
                        anchors.horizontalCenter: parent.horizontalCenter
                        name: "edit"
                        height: units.gu(3)
                        width: units.gu(3)
                    }
                    Text {
                        anchors.top: iconWidget.bottom
                        horizontalAlignment: Text.AlignHCenter
                        width: parent.width
                        text: i18n.tr("Manage widget")
                        font.pointSize: units.gu(1)
                        wrapMode:Text.WordWrap
                    }
                  MouseArea {
                            anchors.fill: parent
                            onPressed: {
                                    //pageStack.push(Qt.resolvedUrl("Widgetmanagement.qml"))
                                    bottomBarSettings.close()
                            }
                  }
                }*/
                Column {
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height
                    width: units.gu(8)
                    Icon {
                        id: iconPage
                        anchors.horizontalCenter: parent.horizontalCenter
                        name: "browser-tabs"
                        height: units.gu(3)
                        width: units.gu(3)
                    }
                    Text {
                        anchors.top: iconPage.bottom
                        horizontalAlignment: Text.AlignHCenter
                        width: parent.width
                        text: i18n.tr("Manage page")
                        font.pointSize: units.gu(1)
                        wrapMode:Text.WordWrap
                    }
                  MouseArea {
                            anchors.fill: parent
                            onPressed: {
                                    pageStack.push(Qt.resolvedUrl("Pagemanagement.qml"))
                                    bottomBarSettings.close()
                            }
                  }
                }
                Column {
                    anchors.verticalCenter: parent.verticalCenter
                    height: parent.height
                    width: units.gu(8)
                    Icon {
                        id: iconSettings
                        anchors.horizontalCenter: parent.horizontalCenter
                        name: "properties"
                        height: units.gu(3)
                        width: units.gu(3)
                    }
                    Text {
                        anchors.top: iconSettings.bottom
                        horizontalAlignment: Text.AlignHCenter
                        width: parent.width
                        text: i18n.tr("Configure Launcher")
                        font.pointSize: units.gu(1)
                        wrapMode:Text.WordWrap
                    }
                  MouseArea {
                            anchors.fill: parent
                            onPressed: {
                                    pageStack.push(Qt.resolvedUrl("Settings.qml"))
                                    bottomBarSettings.close()
                            }
                  }


                }

            }
    }





/* ******************************** FIN DE LA BARRE EN BAS ******************************** */


    }
   }

}
