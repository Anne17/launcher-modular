import QtQuick 2.4
import Ubuntu.Components 1.3
import Ubuntu.Components.ListItems 1.3 as ListItem
import QtGraphicalEffects 1.0
import MySettings 1.0
import AppHandler 1.0
import GSettings 1.0

Item {
    id: widgetFavoriteApps
    width: parent.width
    height: if(AppHandler.fav_appsinfo.length != "0"){titleFavoriteApp.height+rectAppFavoriteView.height}else{units.gu(0)}
    visible: if(searchField.text.length > 0){false}else{true}
    
             Timer {
                id: closeTimer
                    interval: 500; running: home.reloading; repeat: false
                    onTriggered: AppHandler.setFav(settings.coreApps.join())
                }
                
                GSettings {
                    id: settings
                    schema.id: "com.canonical.Unity.ClickScope"        
                    onChanged: AppHandler.setFav(settings.coreApps.join())
                    Component.onCompleted: AppHandler.setFav(settings.coreApps.join())
                }
    
                    Item{
                        id: titleFavoriteApp
                        height: if(AppHandler.fav_appsinfo.length != "0"){ units.gu(5)}else{units.gu(0)}
                        visible: if(AppHandler.fav_appsinfo.length != "0"){ true }else{ false }
                        width: parent.width
                        anchors {
                            left: parent.left
                            leftMargin: units.gu(2)
                        }
                        
                        Icon {
                           id: iconFavoriteApp
                            width: units.gu(2)
                            height: units.gu(2)
                            name: "starred"
                            color: launchermodular.settings.textColor
                        }

                        Label {
                            anchors.left: iconFavoriteApp.right
                            anchors.leftMargin: units.gu(1)
                            text: i18n.tr("Favorite Apps")
                            color: launchermodular.settings.textColor
                        }
                    }
    
                Rectangle {
                    id: rectAppFavoriteView
                    anchors.top: titleFavoriteApp.bottom
                    anchors.horizontalCenter: parent.horizontalCenter
                    width: launchermodular.width
                    height: if(AppHandler.fav_appsinfo.length != "0"){ units.gu(13)}else{units.gu(0)}
                    color: "transparent"

                    
    
                    ListView {
                        id: appFavoriteView
                        model: AppHandler.fav_appsinfo.length
                        anchors.fill: parent
                        clip: true
                        orientation: ListView.Horizontal
                        delegate: Item{
                            width: gview.cellWidth
                            height:units.gu(13)

                            property var fav: AppHandler.fav_appsinfo[index]

                            Image {
                                id: screenshotAppFavorite
                                anchors.right: parent.right
                                width: units.gu(8)
                                height: units.gu(8)
                                source: fav.icon
                                visible: if (launchermodular.settings.iconStyle == "none") { true;}else{ false;}
                            }

                            OpacityMask {
                                anchors.fill: screenshotAppFavorite
                                source: screenshotAppFavorite
                                maskSource: Rectangle {
                                    width: screenshotAppFavorite.width
                                    height: screenshotAppFavorite.height
                                    radius: units.gu(8)
                                    color: if (launchermodular.settings.iconStyle == "rounded") { "";}else{ "transparent";}
                                    visible: if (launchermodular.settings.iconStyle == "rounded") { false;}else{ true;}
                                }
                            }

                            UbuntuShape {
                                source: screenshotAppFavorite
                                anchors.right: parent.right
                                aspect: UbuntuShape.Flat
                                width: if (launchermodular.settings.iconStyle == "default") { units.gu(8);}else{ units.gu(0);}
                                height: if (launchermodular.settings.iconStyle == "default") { units.gu(8);}else{ units.gu(0);}
                                radius : "medium"


                            }
                            MouseArea {
                                    anchors.fill: parent
                                    onClicked: listColumnApps.doAction(fav.action)
                            }


                            Text{
                                anchors.top: screenshotAppFavorite.bottom
                                horizontalAlignment: Text.AlignHCenter
                                anchors.topMargin: units.gu(1)
                                anchors.right: parent.right
                                width: screenshotAppFavorite.width+2
                                font.pixelSize: units.gu(1.5)
                                wrapMode: Text.WordWrap
                                text: fav.name
                                color: launchermodular.settings.textColor
                             }
                        }
                    }
                        

                }
}
